ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!
class ActiveSupport::TestCase
  # Настроить все точки входа в test/fixtures/*.yml для всех тестов в алфавитном порядке.
  fixtures :all
  # Вспомогательные методы, используемые всеми тестами, следует добавлять сюда...
  # Run tests in parallel with specified workers
#  parallelize(workers: 1) #here
end
